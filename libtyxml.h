// This file is part of tyrss.
//
// tyrss is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// tyrss is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with tyrss. If not, see <https://www.gnu.org/licenses/>.

#ifndef TYXML_H
#define TYXML_H 1

#include <stdio.h>

#define BUF_SIZE_INITIAL 64

enum TXEvent {
	ATTR_END,
	ATTR_NAME_END,
	ATTR_START,
	FILE_BEGIN,
	FILE_END,
	TAG_START,
	TAG_NAME_END,
	TAG_END,
};

struct TXParser {
	enum TXEvent event;
	FILE *file;
	_Bool in_cdata;
	_Bool in_comment;
	int prev_1;
	int prev_2;
};

char *tx_advance(struct TXParser *parser, const _Bool capture);

void tx_advance_until(struct TXParser *parser, enum TXEvent event, const char **keys);

/* This function initializes a parser.
 * The caller is responsible for freeing the file as usual in addition to the returned struct.
 */
struct TXParser *tx_init(FILE *file);

/* frees parser and parser->file */
void tx_free(struct TXParser *parser);

#endif
