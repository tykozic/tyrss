# tyrss

I'm Ty. This is my personal RSS reader. I got frustrated with libxml2, so I wrote my own naive parser. Contact me for further information.

## TODO
- [ ] make parser play nice with XML comments
- [ ] add tag/label functionality

## License

tyrss is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

tyrss is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with tyrss. If not, see <https://www.gnu.org/licenses/>.
