// This file is part of tyrss.
//
// tyrss is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// tyrss is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with tyrss. If not, see <https://www.gnu.org/licenses/>.

#include <stddef.h>

#include "libtyxml.h"

/*------BEGIN USER CONFIG------*/

/* directory used for caching feeds */
#define PATH_CACHE "/home/ty/.local/share/rss/"

/* OPML file defining feeds to get */
/* NOTE: most browsers can redirect links that erroneously contain "www." at the beginning. libcurl cannot, so be sure that the URLs you put here are correct. */
/* NOTE: this program currently expects attributes of <outline> tags to be in alphabetical order */
#define PATH_OPML "/home/ty/.config/feeds.opml"

/* preferred formatting for feed dates; see strftime(3) for details.
 * Keep the length updated manually based on the character count incurred by the format.
 * NOTE: only year, month, and day are usable because of the limited information parsed from dates.
 */
#define DATE_FORMAT "%m-%d"
#define DATE_FORMAT_LEN 5

/*------END USER CONFIG------*/

/* these should be null-terminated for easy looping */
static const char *keys_content[]    = { "contents", "description", "summary", NULL }; /* TODO: check if "contents" is correct */
static const char *keys_date[]       = { "pubDate", "published", "updated", "lastBuildDate", "dc:date", NULL };
static const char *keys_entry[]      = { "entry", "item", NULL };
static const char *keys_link_xml[]   = { "xmlUrl", NULL };
static const char *keys_outline[]    = { "outline", NULL };
static const char *keys_text[]       = { "text", NULL };
static const char *keys_title[]      = { "title", NULL };

_Bool entry_print_content(const char *feed_key, int entry_index);
_Bool entry_print_link(const char *feed_key, int entry_index);
_Bool feed_print_entries(const char *feed_key);
_Bool feeds_print(void);
_Bool feeds_update(void);
char *format_date(const char *in);
struct TXParser *parser_init_feed(const char *title);
struct TXParser *parser_init_opml(void);
_Bool opml_advance_to_title(struct TXParser *parser, const char *key);
char *opml_get_title(const char *key);
